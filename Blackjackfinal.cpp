// Blackjackfinal.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <iostream>
#include <string>
#include <windows.h>


using namespace std;

//Variables de uso
int picas[13];
int diamantes[13];
int Corazones[13];
int Treboles[13];
bool gameOver = false;

//Variables Jugador 1
string player1;
int pointsPlayer1 = 0;
bool surrender1 = false;
bool winnerP1 = false;
bool loserP1 = false;

//Variables Jugador 2
string player2 = "Croupiere";
int pointsPlayer2 = 0;
bool surrender2 = false;
bool winnerP2 = false;
bool loserP2 = false;



//Funciones

//Inicializa cualquier array con sus 12 posiciones a 1
//de esta forma, indicamos que todas las cartas están disponibles
//si una posición está a 0, esa carta ya está repartida y no se repite


void gameStart() {
    cout << "!EL BLACKJACK VA COMENZAR!\n";
    cout << "_________________________\n";
    cout << "\n";
    cout << "Como se llama el Jugador:\n";
    cin >> player1;
    cout << "El nombre del jugador es " << player1 << "\n";
}


void inicializaArray(int(&pArray)[13]) {
    for (int i = 0; i < 13; i++) {
        pArray[i] = 1;
    }
}

//Comprueba aleatoriamente si ha de dar picas o diamantes
//y busca una carta al azar. Hasta que no encuentre una posición
// a 1, no asigna esa carta, cuyo valor es el indiceCarta + 1

void chooseCards(int& totalPoints, string player) {
    bool choosed = false;
    string tipoPalo;
    int indiceCarta = 0;
    int indicePalo = 0;
    int eleccion = 0;


    while (!choosed) {
        //Elije una carta del 1 al 13
        indiceCarta = rand() % 13;
        //ELIJE EL PALO
        indicePalo = rand() % 4;
        //Comprueba las Copas
        if (indicePalo == 0) {
            if (picas[indiceCarta] == 1) {
                //devuelve la carta
                picas[indiceCarta] = 0;
                choosed = true;
                tipoPalo = "picas";
            }
            //Comprueba los diamantes
        }
        else if (indicePalo == 1) {
            if (diamantes[indiceCarta] == 1) {
                //devuelve la tarjeta
                diamantes[indiceCarta] = 0;
                choosed = true;
                tipoPalo = "diamantes";
            }
        }
        else if (indicePalo == 2) {
            if (Corazones[indiceCarta] == 1) {
                //devuelve la tarjeta
                Corazones[indiceCarta] = 0;
                choosed = true;
                tipoPalo = "Corazones";
            }
        }
        else {
            if (Treboles[indiceCarta] == 1) {
                //devuelve la tarjeta
                Treboles[indiceCarta] = 0;
                choosed = true;
                tipoPalo = "Treboles";
            }
        }
    }
    if (player == player1) {

        if (indiceCarta == 1) {
            //LÓGICA DEL JUGADOR            
            do {

                cout << "El jugador " << player << " ha sacado un AS de " << tipoPalo << " y por ahora tiene un total de " << totalPoints << " Puntos " << endl;
                cout << "Que numero quieres elegir el 1 o 11?" << endl;
                cin >> eleccion;
                if (eleccion == 1) {
                    totalPoints += 1;
                }
                else if (eleccion == 11) {
                    totalPoints += 11;
                }
                else {
                    cout << "Numero incorrecto vuelve a introducir un 1 o un 11 " << endl;
                }
            } while (eleccion != 1 && eleccion != 11);
        }
        else if (indiceCarta == 11) {
            //LÓGICA DEL JUGADOR            
            totalPoints += 10;
            cout << "El jugador " << player << " ha sacado un J de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
        else if (indiceCarta == 12) {
            //LÓGICA DEL JUGADOR            
            totalPoints += 10;
            cout << "El jugador " << player << " ha sacado un Q de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
        else if (indiceCarta == 13) {
            //LÓGICA DEL JUGADOR            
            totalPoints += 10;
            cout << "El jugador " << player << " ha sacado un K de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
        else {
            totalPoints += indiceCarta + 1;
            cout << "El jugador " << player << " ha sacado un " << indiceCarta + 1 << " de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;

        }
    }
    if (player == "Croupiere") {
        if (indiceCarta == 1) {
            if (totalPoints <= 10) {
                //ELIGE UN 11;                
                totalPoints += 11;
            }
            else {
                totalPoints += 1;
            }
            cout << "El jugador " << player << " ha sacado un AS de " << tipoPalo << " y tiene un total de " << totalPoints << endl;
        }
        else if (indiceCarta == 11) {
            totalPoints += 10;
            cout << "El jugador " << player << " ha sacado un J de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
        else if (indiceCarta == 12) {
            totalPoints += 10;
            cout << "El jugador " << player << " ha sacado un Q de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
        else if (indiceCarta == 13) {
            totalPoints += 10;
            cout << "El jugador " << player << " ha sacado un K de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
        else {
            totalPoints += indiceCarta + 1;
            cout << "El jugador " << player << " ha sacado un " << indiceCarta + 1 << " de " << tipoPalo << " y tiene un total de " << totalPoints << " Puntos " << endl;
        }
    }

}

bool checkPoints(int& points, bool& winner, bool& loser) {
    if (points == 21) {
        winner = true;
        return true;
    }
    else if (points > 21) {
        loser = true;
        return true;
    }
    return false;
}

bool checkWhoWins(int points1, int points2, bool& winner2) {
    if (points2 > points1) {
        winner2 = true;
        return true;
    }
    else {
        return false;
    }
}
bool checkRepeticionCarta(int num, int indiceCarta[])
{
    for (int i = 0; i < 13; i++)
        if (num == indiceCarta[i])
            return true;
    return false;
}


int main() {
    system(" Color 5A");
    gameStart();
    Sleep(500);
    inicializaArray(picas);
    inicializaArray(diamantes);
    inicializaArray(Corazones);
    inicializaArray(Treboles);
    srand(time(NULL));
    Sleep(1000);
    chooseCards(pointsPlayer1, player1);
    chooseCards(pointsPlayer1, player1);
    Sleep(1000);


    //Puede pasar que ganemos o perdamos nada más comenzar
    surrender1 = checkPoints(pointsPlayer1, winnerP1, loserP1);
    chooseCards(pointsPlayer2, player2);
    while (!surrender1) {
        char choice = '0';
        //Comienza el juego con el jugador 1
        while (choice != '1' && choice != '2') {
            cout << "Tienes estos puntos " << pointsPlayer1 << ", Quieres pedir carta?" << endl;
            cout << "1.- Si" << endl;
            cout << "2.- No" << endl;
            cin >> choice;
            if (choice != '1' && choice != '2') {
                cout << "Por favor, elije una opcion valida." << endl;
            }

        }
        if (choice == '1') {
            chooseCards(pointsPlayer1, player1);
            surrender1 = checkPoints(pointsPlayer1, winnerP1, loserP1);
            //comprueba si se ha pasado o ha ganado la partida
        }
        else {
            //Si no quiere carta, es turno de la banca
            surrender1 = true;
        }
    }
    //Termina el turno del jugador 1. Hay que comprobar si ha ganado (winnerP1) o perdido ya (loserP1), o si la banca tiene que tirar
    if (winnerP1) {
        cout << "El jugador " << player1 << " gana y la banca pierde" << endl;
    }
    else if (loserP1) {
        cout << "El jugador " << player1 << " pierde y la banca gana" << endl;
    }
    else {
        //turno de la banca
        while (!surrender2) {
            chooseCards(pointsPlayer2, player2);

            surrender2 = checkPoints(pointsPlayer2, winnerP2, loserP2);
            if (!surrender2) {
                //Comprueba si la banca ya gana por puntos
                surrender2 = checkWhoWins(pointsPlayer1, pointsPlayer2, winnerP2);
            }
        }
        if (winnerP2) {
            cout << "La banca gana y el jugador " << player1 << " pierde" << endl;
        }
        else if (loserP2) {
            cout << "La banca pierde y el jugador " << player1 << " gana" << endl;
        }
    }

}


// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
